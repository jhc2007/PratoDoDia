<?php

	// A ENVIAR:



	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= 'From: <geral@pratododia.org>' . "\r\n";
	// $headers .= 'From: <geral@pratododia.net78.net>' . "\r\n";

	$linkApp = "https://play.google.com/store/apps/details?id=org.pratododia.pratododia&hl=pt_PT";
	
	$subject = "Prato do Dia convida ";

	$today = date_create(NULL, timezone_open('Europe/Lisbon'));

	if(date_format($today, 'H') > 12) 
		$greeting = "<p>Boa tarde,<br><br></p>";
	else
		$greeting = "<p>Bom dia,<br><br></p>";
		
	foreach ($restaurant as $rest) {

		$restName = substr($rest[0], 0, 5) . htmlentities(substr($rest[0], 5, -4)) . substr($rest[0], -4);
			
		// $message = "<p>Bom dia,<br><br></p>";
		$message = $greeting;
		$message .= "<p>O <b>Prato do Dia</b> &eacute; uma plataforma, de utiliza&ccedil;&atilde;o inteiramente gratuita, que se destina &agrave; divulga&ccedil;&atilde;o de pratos do dia.</p>";
		$message .= "<p>A ades&atilde;o ao servi&ccedil;o e a publica&ccedil;&atilde;o dos pratos do dia por parte dos restaurantes devem ser feitas em <a style='text-decoration: none;' href='http://www.pratododia.org' target='_blank'>www.pratododia.org</a>.</p>";
		$message .= "<p>As ementas di&aacute;rias disponibilizados pelos diversos restaurantes podem depois ser consultadas atrav&eacute;s da aplica&ccedil;&atilde;o Android: <a style='text-decoration: none;' href='" . $linkApp . "' target='_blank'>PratoDoDia</a>.</p>";
		$message .= "<p>&Eacute; com enorme prazer que a equipa do Prato do Dia vem, desta forma, convidar " . $restName . " a aderir e participar neste projecto pioneiro.</p>";
		$message .= "<p>Para qualquer esclarecimento adicional aqui fica o nosso endere&ccedil;o de email: <a style='text-decoration: none;' href='mailto:geral@pratododia.org' target='_blank'>geral@pratododia.org</a>.</p>";
		$message .= "<p>Resta-nos agradecer a aten&ccedil;&atilde;o dedicada a esta nossa apresenta&ccedil;&atilde;o.<br><br></p>";
		$message .= "<p>At&eacute; breve,";
		// $message .= "<p><a href='http://www.pratododia.org' target='_blank' style='text-decoration: none;'><img src='png/logo_email.png'/><br><b>Prato do Dia</b></a></p>";
		$message .= "<br><a style='text-decoration: none;' href='http://pratododia.org/' target='_blank'><b>Prato do Dia.</b></a></p>";

		echo $restName . " " . $rest[1]."<br>";
		// mail($rest[1], $subject . substr($rest[0], 5, -4), $message, $headers);

	}

	echo $message;
?>