<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="description" content="O Prato do Dia é uma plataforma para divulgação de pratos do dia.">
	<meta name="keywords" content="prato do dia, diária, almoço, diário, menu, ementa, esposende">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
	<script src="js/pratododia.js"></script>
</head>
<body>
<?php
	if(!isset($_SESSION))
		session_start();
	session_unset();
	session_destroy();
?>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<!-- <form name="formlogin" id="formlogin" action="login.php" onsubmit="return loginValidation()" method="post"> -->
		<form class="w3-form" action="login.php" onsubmit="return emailValidation();" method="post">
			<div class="w3-group">      
				<input class="w3-input" type="email" name="inputEmail" id="inputEmail" onfocus="clearErrorMessage()" required>
				<label class="w3-label">Email</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="password" name="inputPassword" onfocus="clearErrorMessage()" required>
				<label class="w3-label">Password</label>
			</div>
			<p id="pErrorMessage" class="pErrorMessage">
				<?php
					if(isset($_GET['e']) && $_GET['e'] == 1)
						echo "Email / Password incorrectos";
					unset($_GET);
				?>
			</p>
			<div style="text-align: right;">
				<button class="w3-btn w3-blue-grey" type="submit">Entrar</button>
			</div>
		</form>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-half w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="sendrecoveryemail.php">Recuperar Password</a>
		</div>
		<div class="w3-half w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="registration.php">Criar Registo</a>
			<!-- <button class="w3-btn " type="submit">Criar Novo Registo</button> -->
		</div>
	</div>
	<div class="w3-container" id="divDescription">
		<p>
		O <b>Prato do Dia</b> &eacute; uma plataforma que se destina &agrave; divulga&ccedil;&atilde;o de pratos do dia.
		</p><p>
		Depois de se registarem, os restaurantes aderentes podem publicar aqui as suas ementas di&aacute;rias.
		</p><p>
		A consulta dos pratos do dia disponibilizados pelos diversos restaurantes &eacute; feita atrav&eacute;s da aplica&ccedil;&atilde;o Android:<br>
		</p>
		<p style="text-align: center;">
		<a href="https://play.google.com/store/apps/details?id=org.pratododia.pratododia&hl=pt_PT" target="_blank"><img src="png/logo.png"/><br>Prato do Dia</a>
		</p>
	</div>
	<!-- <a href="http://www.000webhost.com/" target="_blank"><img src="http://www.000webhost.com/images/80x15_powered.gif" alt="Web Hosting" width="80" height="15" border="0" /></a> -->
</div>
<!-- <a id="p000WebHost" href="http://www.000webhost.com/" target="_blank"><img src="http://www.000webhost.com/images/120x60_powered.gif" alt="Web Hosting" width="120" height="60" border="0" /></a> -->
<!-- <p id="p000WebHost"><a href="http://www.000webhost.com/" target="_blank"><img src="http://www.000webhost.com/images/80x15_powered.gif" alt="Web Hosting" width="80" height="15"/></a></p> -->
</body>
</html>