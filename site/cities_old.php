﻿<?php

	$cities = array("Esposende",
					"Viana do Castelo",
					"Póvoa de Varzim",
					"Maia",
					"Vila do Conde",
					"Barcelos",
					"Braga",
					"Matosinhos",
					"Porto",
					"Ponte de Lima",
					"Guimarães",
					"Vila Nova de Famalicão",
					"Santo Tirso",
					"Vila Nova de Gaia");

	// sort($cities);
	natcasesort($cities);
	
	foreach ($cities as &$v) {
		$v = htmlentities($v);
	}
?>