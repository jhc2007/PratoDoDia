<?php

	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= 'From: <geral@pratododia.org>' . "\r\n";
	// $headers .= 'From: <geral@pratododia.net78.net>' . "\r\n";

	$linkApp = "https://play.google.com/store/apps/details?id=org.pratododia.pratododia&hl=pt_PT";

	// ENVIADO 
	// 16 -> buxa.restaurantesnack@gmail.com
	// 24 -> Cafe.pinhal@sapo.pt
	// 27 -> restaurante100stress@sapo.pt
	// 30 -> cerejocatering@gmail.com
	// 31 -> jacaestourestaurantebar@gmail.com

	$email = "jhc2007@gmail.com";
	// $email = "pontuel@pontuel.pt";
	// $email = "restaurante.o.menino@gmail.com";
	
	$subject = "Prato do Dia";
	
	$message = "<p>Boa noite,<br><br></p>";
	$message .= "<p>Desde j&aacute; agradecemos o interesse demonstrado pelo servi&ccedil;o de divulga&ccedil;&atilde;o <b>Prato do Dia</b>.</p>";
	
	$message .= "<p>Este servi&ccedil;o &eacute; constitu&iacute;do por uma plataforma (<a style='text-decoration: none;' href='http://www.pratododia.org' target='_blank'>www.pratododia.org</a>) que permite aos restaurantes aderentes publicarem as suas ementas di&aacute;rias.</p>";
	$message .= "<p>Paralelamente, o sistema inclui uma aplica&ccedil;&atilde;o para dispositivos Android (<a style='text-decoration: none;' href='" . $linkApp . "' target='_blank'>PratoDoDia</a>) destinada &agrave; consulta dos pratos do dia disponibilizados pelos diversos restaurantes.</p>";
	$message .= "<p>A transfer&ecirc;ncia e instala&ccedil;&atilde;o da aplica&ccedil;&atilde;o podem ser feitas atrav&eacute;s da <i>Play Store</i> (app presente em qualquer dispositivo Android) ou diretamente atrav&eacute;s de: <a style='text-decoration: none;' href='" . $linkApp . "' target='_blank'>" . $linkApp . "</a>.</p>";
	
	// $message .= "<p>A visualiza&ccedil;&atilde;o dos menus di&aacute;rios atrav&eacute;s da aplica&ccedil;&atilde;o <a style='text-decoration: none;' href='" . $linkApp . "' target='_blank'>PratoDoDia</a> s&oacute; &eacute; poss&iacute;vel ap&oacute;s o preenchimento dos campos relativos &agrave; refei&ccedil;&atilde;o.</p>";
	// $message .= "<p>Para isso, dever&aacute; aceder a <a style='text-decoration: none;' href='http://pratododia.org' target='_blank'>pratododia.org</a>, efectuar o login, clicar em 'Editar Refei&ccedil;&atilde;o', preencher os respectivos campos e, finalmente, clicar em 'Guardar'.<br>Depois deste procedimento, os pratos do dia surgem automaticamente na aplica&ccedil;&atilde;o.</p>";
	// $message .= "<p>Para inserir novos menus dever&aacute; clicar apenas em 'Inserir Pratos do Dia', n&atilde;o sendo necess&aacute;rio repetir o procedimento anterior.</p>";
	// $message .= "<p>Em caso de d&uacute;vida n&atilde;o hesite em contactar-nos novamente.<br><br></p>";
	
	$message .= "<p>Em caso de d&uacute;vida n&atilde;o hesite em contactar-nos novamente.<br><br></p>";
	$message .= "<p>At&eacute; breve,";
	$message .= "<br><a style='text-decoration: none;' href='http://pratododia.org/' target='_blank'><b>Prato do Dia.</b></a></p>";



	echo $email ."<br>";
	mail($email, $subject, $message, $headers);


	echo $message;
?>