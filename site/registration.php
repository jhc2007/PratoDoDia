<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
	<script src="js/pratododia.js"></script>
	<script src="js/cities.js"></script>
</head>
<body>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<!-- <h5>Formul&aacute;rio de Registo</h5> -->
		 <!-- name="formLogin" id="formLogin"  -->
		<form class="w3-form" action="insert_registration.php" onsubmit="return (emailValidation() && wordValidation());" method="post">
			<div class="w3-group">      
				<input class="w3-input" type="text" name="inputName" maxlength='50' onfocus="clearErrorMessage()" required>
				<label class="w3-label">Nome do Restaurante</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="text" name="inputAddress" maxlength='100' onfocus="clearErrorMessage()">
				<label class="w3-label">Morada</label>
			</div>
			<div class="w3-group">      
				<select class="w3-input" name="selectDistrict" onfocus="clearErrorMessage()" onChange="getCities(this.value)" required>
					<?php
						include('districts.php');
						for ($i=0; $i < count($districts); $i++) { 
							echo "<option value='" . $i . "'>" . $districts[$i] . "</option>";
						}
						// include('districts.php');
						// // $index = 0;
						// foreach($districts as $key => $val) {
						// 	echo "<option value='" . $key . "'>" . htmlentities($key) . "</option>";
						// 	// $index++;
						// }
					?>
				</select>
				<label class="w3-label">Distrito</label>
			</div>
			<div class="w3-group">      
				<select class="w3-input" name="selectCity" id="selectCity" onfocus="clearErrorMessage()" required>
					<?php
						include('cities.php');
						foreach($cities[0] as $val) {
							echo "<option value='" . $val . "'>" . $val . "</option>";
						}
						// foreach($districts['Aveiro'] as $city)
						// 	echo "<option value='" . $city . "'>" . htmlentities($city) . "</option>";
						// // include('cities.php');
						// // foreach($cities as $city) {
						// // 	echo "<option value='" . $city . "'>" . $city . "</option>";
						// // }
					?>
				</select>
				<label class="w3-label">Concelho</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="tel" pattern='\d{9}' name="inputTelephone" maxlength='9' onfocus="clearErrorMessage()">
				<label class="w3-label">Telefone</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="email" name="inputEmail" id="inputEmail" maxlength='50' onfocus="clearErrorMessage()" required>
				<label class="w3-label">Email</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="password" name="inputPassword" maxlength='50' onfocus="clearErrorMessage()" required>
				<label class="w3-label">Password</label>
			</div>
				<?php
					//If file exist...
					if(file_exists("txt/captcha_words.txt")){
					    //Read the file
					    $f_contents = file("txt/captcha_words.txt");
					    //Extract a word and replace CrLf by empty
					    $word = str_replace("%0D%0A", "", trim($f_contents[array_rand($f_contents)]));
					    //Length of the word?
					    // $len = strlen(utf8_decode($word));
					    //Random number
					    // $nb = rand(1, $len);
					    //Take the letter
					    //(-1 because function substr start with 0)
					    // $reply = substr($word, $nb-1, 1);  
					    //Place it in a session
				  //   	if (!isset($_SESSION)) {
						// 	session_start();
						// }
					 //    $_SESSION['color'] = $color;
					}
				?>
			<div class="w3-group">      
				<input class="w3-input" type="number" id="inputNumberOfLetters" onfocus="clearErrorMessage()" required>
				<label class="w3-label">N&uacute;mero de letras da palavra '<b id="word"><?php echo $word; ?></b>'</label>
			</div>
			<p id="pErrorMessage" class="pErrorMessage">
				<?php
					if(isset($_GET['e'])) {
						if($_GET['e'] == 1)
							echo "Email j&aacute; existente na base de dados";
						unset($_GET);
					} 
				?>
			</p>
			<div style="text-align: right;">
				<button class="w3-btn w3-blue-grey" type="submit">Guardar</button>
			</div>
		</form>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="index.php">Voltar</a>
		</div>
	</div>
</div>
</body>
</html>