<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
</head>
<body>
<?php
	if (!isset($_SESSION))
		session_start();

	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
		die();
	}
?>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<!-- <div class="w3-container w3-border" style="margin:0.5em;"> -->
			<a class="w3-btn w3-light-grey w3-border buttonBottom" style="margin-top:1em;" href="updateregistration.php">Editar Registo</a>
		<!-- </div> -->
		<!-- <div class="w3-container w3-border" style="margin:0.5em;"> -->
			<a class="w3-btn w3-light-grey w3-border buttonBottom" style="margin-top:1em;" href="description.php">Editar Refei&ccedil;&atilde;o</a>
		<!-- </div> -->
		<!-- <div class="w3-container w3-border" style="margin:0.5em;"> -->
			<a class="w3-btn w3-light-grey w3-border buttonBottom" style="margin-top:1em;margin-bottom:1em;" href="dishes.php">Inserir Pratos do Dia</a>
		<!-- </div> -->
		<!-- <div class="w3-container w3-border" style="margin:0.5em; width:100%;"> -->
<!-- 			<a class="w3-btn w3-light-grey w3-border buttonBottom" style="margin-top:1em;margin-bottom:1em;" href="index.php">Sair</a> -->
		<!-- </div> -->
	</div>
		<div class="w3-container divBottom">
		<div class="w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="index.php">Sair</a>
		</div>
	</div>
</div>
</body>
</html>