<?php
	
	if(isset($_GET['city']) && isset($_GET['date']) && isset($_GET['weekend'])) {

		header("Content-Type: text/html; charset=UTF-8", true);
		
		$city = $_GET['city'];
		$date = $_GET['date'];
		$weekend = $_GET['weekend'];
		
		include('connect_db.php');	

		//echo "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

		mysql_set_charset('utf8');

		// $result = mysql_query("SELECT Nome, Prato, Morada, Concelho, Telefone, Descricao, Preco FROM RESTAURANTE, PRATODODIA, REFEICAO WHERE RESTAURANTE.`ID_Restaurante` = PRATODODIA.`ID_Restaurante` AND PRATODODIA.`ID_Restaurante` = REFEICAO.`ID_Restaurante` AND Data = '2015-10-22' ORDER BY Nome;");

		$result = mysql_query("SELECT Nome, Prato, Morada, Telefone, Descricao, PrecoSemana, PrecoFimSemana FROM RESTAURANTE, PRATODODIA, REFEICAO WHERE RESTAURANTE.`ID_Restaurante` = PRATODODIA.`ID_Restaurante` AND PRATODODIA.`ID_Restaurante` = REFEICAO.`ID_Restaurante` AND Concelho = '$city' AND Data = '$date' AND RESTAURANTE.`DataPagamento` <> 'NULL' AND DATE_SUB(CURDATE(), INTERVAL 366 DAY) <= RESTAURANTE.`DataPagamento` ORDER BY Nome;");
		//dbConnectionError($result, $connection);
		if(!$result || mysql_num_rows($result) == 0) {
			mysql_close($connection);
			print(json_encode(null));
    		die();
		}

		$response = array();

		while($data = mysql_fetch_assoc($result)) {

			// $res = array("Nome" => $data['Nome'], "Prato" => $data['Prato'], "Descricao" => $data['Descricao']);
			$res = array("Nome" => $data['Nome'], "Prato" => $data['Prato']);

			if($data['Descricao'] != NULL)
				$res["Descricao"] = $data['Descricao'];
			else
				$res["Descricao"] = "";

			if($data['Morada'] != NULL)
				$res["Morada"] = $data['Morada'];

			if($data['Telefone'] != NULL)
				$res["Telefone"] = $data['Telefone'];

			if($weekend && $data['PrecoFimSemana'] != NULL)
				$res["Preco"] = $data['PrecoFimSemana'];
			elseif($data['PrecoSemana'] != NULL)
				$res["Preco"] = $data['PrecoSemana'];
			else
				$res["Preco"] = "0.00";

			array_push($response, $res);
			//$response[] = $data;

		}

		// echo $response[1]["Nome"];

		include('disconnect_db.php');

		print(json_encode($response));
	}
	// unset($_GET);

?>