<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
	<script src="js/pratododia.js"></script>
	<script src="js/cities.js"></script>
</head>
<body>
<?php
	if (!isset($_SESSION)) {
		session_start();
	}
	if(isset($_SESSION['id'])) {

		$id = $_SESSION['id'];

		include('connect_db.php');

		$result = mysql_query("SELECT Nome, Morada, Distrito, Concelho, Telefone, Email FROM RESTAURANTE WHERE ID_Restaurante = $id");
		dbConnectionError($result, $connection);

		if(mysql_num_rows($result) > 0) {
			$data = mysql_fetch_array($result);
			$name = $data['Nome'];
			if($data['Morada'] == 'NULL')
				$address = "";
			else
				$address = $data['Morada'];
			$district = $data['Distrito'];
			$city = $data['Concelho'];
			if($data['Telefone'] == 'NULL')
				$tel = "";
			else
				$tel = $data['Telefone'];
			$email = $data['Email'];
		}

		include('disconnect_db.php');
		
	} else {
		header('Location: index.php');
		die();
	}
?>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<!-- <h5>Formul&aacute;rio de Registo</h5> -->
		 <!-- name="formLogin" id="formLogin"  -->
		<form class="w3-form" action="update_registration.php" onsubmit="return emailValidation();" method="post">
			<div class="w3-group">      
				<input class="w3-input" type="text" name="inputName" value="<?php echo $name; ?>" maxlength='50' onfocus="clearErrorMessage()" required>
				<label class="w3-label">Nome do Restaurante</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="text" name="inputAddress" value="<?php echo $address; ?>" maxlength='100' onfocus="clearErrorMessage()">
				<label class="w3-label">Morada</label>
			</div>
			<div class="w3-group">      
				<select class="w3-input" name="selectDistrict" id="selectDistrict" onfocus="clearErrorMessage()" onChange="getCities(this.value)" required>
					<?php
						include('districts.php');
						for ($i=0; $i < count($districts); $i++) { 
							if(strcmp(htmlentities($district, ENT_COMPAT, 'ISO-8859-1'), $districts[$i]) == 0)
								echo "<option selected value='" . $i . "'>" . $districts[$i] . "</option>";
							else
								echo "<option value='" . $i . "'>" . $districts[$i] . "</option>";
						}
						// foreach($districts as $val) {
						// 	if(strcmp(htmlentities($district, ENT_COMPAT, 'ISO-8859-1'), htmlentities($val)) == 0)
						// 		echo "<option selected value='" . $val . "'>" . htmlentities($val) . "</option>";
						// 	else
						// 		echo "<option value='" . $val . "'>" . htmlentities($val) . "</option>";
						// }
					?>
				</select>
				<label class="w3-label">Distrito</label>
			</div>
			<div class="w3-group">      
				<select class="w3-input" name="selectCity" id="selectCity" onfocus="clearErrorMessage()" required>
					<?php
						include('cities.php');
						foreach($cities[array_search(htmlentities($district, ENT_COMPAT, 'ISO-8859-1'), $districts)] as $val) {
							// if(strcmp($city, $val) == 0)
							if(strcmp(htmlentities($city, ENT_COMPAT, 'ISO-8859-1'), $val) == 0)
								echo "<option selected value='" . $val . "'>" . $val . "</option>";
							else
								echo "<option value='" . $val . "'>" . $val . "</option>";
						}
					?>
				</select>
				<label class="w3-label">Concelho</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="tel" pattern='\d{9}' name="inputTelephone" value="<?php echo $tel; ?>" maxlength='9' onfocus="clearErrorMessage()">
				<label class="w3-label">Telefone</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="email" name="inputEmail" id="inputEmail" value="<?php echo $email; ?>" maxlength='50' onfocus="clearErrorMessage()" required>
				<label class="w3-label">Email</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="password" name="inputPassword" maxlength='50' onfocus="clearErrorMessage()" required>
				<label class="w3-label">Password</label>
			</div>
			<p id="pErrorMessage" class="pErrorMessage">
				<?php
					if(isset($_GET['e'])) {
						if($_GET['e'] == 1)
							echo "Email j&aacute; existente na base de dados";
						else //e == 2
							echo "Password incorrecta";
					}
					unset($_GET);
				?>
			</p>
			<div style="text-align: right;">
				<button class="w3-btn w3-blue-grey" type="submit">Guardar</button>
			</div>
		</form>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-half w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="updatepassword.php">Alterar Password</a>
		</div>
		<div class="w3-half w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="menu.php">Voltar</a>
		</div>
	</div>
</div>
</body>
</html>