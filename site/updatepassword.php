<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
	<script src="js/pratododia.js"></script>
</head>
<body>
<?php
	if (!isset($_SESSION))
		session_start();

	if(!isset($_SESSION['id'])) {
		header('Location: index.php');
		die();
	}
?>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<form class="w3-form" action="update_password.php" onsubmit="return (newPasswordValidation() && passwordRepetitionValidation());" method="post">
			<div class="w3-group">      
				<input class="w3-input" type="password" name="inputActualPassword" id="inputActualPassword" onfocus="clearErrorMessage()" required>
				<label class="w3-label">Password actual</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="password" name="inputNewPassword" id="inputNewPassword" onfocus="clearErrorMessage()" required>
				<label class="w3-label">Password nova</label>
			</div>
			<div class="w3-group">      
				<input class="w3-input" type="password" id="inputNewPasswordRepetition" onfocus="clearErrorMessage()" required>
				<label class="w3-label">Password nova (repeti&ccedil;&atilde;o)</label>
			</div>
			<p id="pErrorMessage" class="pErrorMessage">
				<?php
					if(isset($_GET['e'])) {
						if($_GET['e'] == 2)
							echo "Password actual incorrecta";
					}
					unset($_GET);
				?>
			</p>
			<div style="text-align: right;">
				<button class="w3-btn w3-blue-grey" type="submit">Guardar</button>
			</div>
		</form>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="menu.php">Voltar</a>
		</div>
	</div>
</div>
</body>
</html>