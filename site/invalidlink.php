<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
</head>
<body>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3 style="font-family: monospace; color: black;">Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<p>Link inv&aacute;lido.</p>
		<p>Por favor, tente recuperar a password novamente.</p>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="index.php">In&iacute;cio</a>
		</div>
	</div>
</div>
</body>
</html>