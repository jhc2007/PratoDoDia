<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
	<script src="js/dishes.js"></script>
</head>
<body>
<?php

	if (!isset($_SESSION)) {
		session_start();
	}
	
	if(isset($_SESSION['id'])) {

		include('connect_db.php');
		
		$id = $_SESSION['id'];

		// date_default_timezone_set('Europe/Lisbon');
		$day = date_create(NULL, timezone_open('Europe/Lisbon'));
		
		// $day1 = date('Y-m-d', $day);
		// 

		// echo "sim:" . date_format($day, "Y/m/d H:i:s") . "<br>";

		if(date_format($day, 'H') > 14) {
			date_modify($day, '+1 day');
		}

		$str = "Data >= '" . date_format($day, 'Y-m-d') . "' AND Data <= '";
		date_modify($day, '+6 day');
		$str .= date_format($day, 'Y-m-d') . "' ORDER BY Data;";
		date_modify($day, '-6 day');

		$result = mysql_query("SELECT * FROM PRATODODIA WHERE ID_Restaurante = $id AND " . $str);
		dbConnectionError($result, $connection);

		$dishes = array();

		while($data = mysql_fetch_array($result)) {
			array_push($dishes, array($data['Data'], $data['Prato']));
		}

		include('disconnect_db.php');

	} else {
		header('Location: index.php');
		die();
	}
?>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		 <!-- onsubmit="return formLoginValidation()" -->
		<form class="w3-form" action="insert_dishes.php" method="post">
<?php
		
	$weekDays = array("Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom");
	
	// echo "nao:" . date_format($day, "Y/m/d");

	echo "<input class='w3-input inputMask' type='text' name='inputMaskDay1' readonly value='" . date_format($day, 'Y-m-d') . "'>";
	// echo "nao: " . strtotime("next Day"), "\n";
	// $day = date('d-m-Y', $day);

	for($i = 0; $i < 7; $i++) { 

		// echo "<br>AQUI:" . $weekDays[date('N', $day) - 1] . "<br>AQUI2:";
		// echo date('d-m-Y', $day) . "<br>";
	    echo "<div class='w3-group w3-border' style='margin-top:0.5em;margin-bottom:0px;'>";
		echo "<a class='w3-btn w3-light-grey buttonDishes' onclick='displayTextarea(" . $i . ")'>" . $weekDays[date_format($day, 'N') - 1] . " " . date_format($day, 'd/m') . "</a>";

		$j = 0;
		while($j < count($dishes) && $dishes[$j][0] < date_format($day, 'Y-m-d'))
			$j++;

		if($j < count($dishes) && $dishes[$j][0] == date_format($day, 'Y-m-d')) {
			echo "<textarea class='w3-input' name='textareaDay" . $i . "' id='textareaDay" . $i . "' rows='5' maxlength='255'>" . $dishes[$j][1] . "</textarea>";
		} else {
			if($id == 15 && date_format($day, 'N') == 6)
				echo "<textarea class='w3-input' name='textareaDay" . $i . "' id='textareaDay" . $i . "' rows='5' maxlength='255'>Encerrado para descanso do pessoal</textarea>";
			else
				echo "<textarea class='w3-input inputMask' name='textareaDay" . $i . "' id='textareaDay" . $i . "' rows='5' maxlength='255'></textarea>";
		}

		echo "</div>";
		date_modify($day, '+1 day');
		// $day = date('Y-m-d H:i:s', strtotime($day . ' + 1 days'));
		// echo "aqui: " . $day;

    	// document.writeln("<div class='w3-group' style='margin:auto;'>");
    	// document.writeln("<a class='w3-btn w3-blue-grey buttonBottom' onclick='displayTextarea(i)'>" + weekDays[day.getDay()] + day.toLocaleDateString() "</a>");
	    // document.writeln("<input class='w3-input inputMask' type='text' name='inputMaskDay" + i + "' readonly value='" + day.toLocaleDateString() + "'>");
	    // document.writeln("<textarea class='w3-input inputMask' name='textareaDay" + i + "' id='textareaDay" + i + "' rows='5'></textarea>");
	    // document.writeln("</div>");

	}
?>

<!-- 			<div class="w3-group" style="margin:auto;">      
				<a class="w3-btn w3-blue-grey buttonBottom" onclick="displayTextarea(1)">Hoje</a>
				<input class="w3-input inputMask" type="text" name="inputMaskDay1" readonly value="2005">
				<textarea class="w3-input inputMask" name="textareaDay1" id="textareaDay1" rows="5"></textarea>
			</div> -->
 			<p id="pErrorMessage" class="pErrorMessage"></p>
			<div style="text-align: right;">
				<button class="w3-btn w3-blue-grey" type="submit">Guardar</button>
			</div>
		</form>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="menu.php">Voltar</a>
		</div>
	</div>
</div>
</body>
</html>