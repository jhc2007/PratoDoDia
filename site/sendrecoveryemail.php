<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
	<script src="js/pratododia.js"></script>
</head>
<body>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<form class="w3-form" action="send_recovery_email.php" onsubmit="return (emailValidation() && wordValidation());" method="post">
			<div class="w3-group">      
				<input class="w3-input" type="email" name="inputEmail" id="inputEmail" onfocus="clearErrorMessage()" required>
				<label class="w3-label">Email</label>
			</div>
			<?php
				if(file_exists("txt/captcha_words.txt")){
				    $f_contents = file("txt/captcha_words.txt");
				    $word = str_replace("%0D%0A", "", trim($f_contents[array_rand($f_contents)]));
				}
			?>
			<div class="w3-group">      
				<input class="w3-input" type="number" id="inputNumberOfLetters" onfocus="clearErrorMessage()" required>
				<label class="w3-label">N&uacute;mero de letras da palavra '<b id="word"><?php echo $word; ?></b>'</label>
			</div>
			<p id="pErrorMessage" class="pErrorMessage">
				<?php
					if(isset($_GET['e']) && $_GET['e'] == 1)
						echo "Email inexistente";
					unset($_GET);
				?>
			</p>
			<div style="text-align: right;">
				<button class="w3-btn w3-blue-grey" type="submit">Enviar</button>
			</div>
		</form>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="index.php">Voltar</a>
		</div>
	</div>
</div>
</body>
</html>