<!DOCTYPE html>
<html lang="pt-PT">
<head>
	<!-- <meta charset="UTF-8"> -->
	<meta charset="iso-8859-1">
	<link rel="shortcut icon" href="ico/logo.ico">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Prato do Dia</title>
	<link rel="stylesheet" href="css/w3.css">
	<link rel="stylesheet" href="css/pratododia.css">
</head>
<body>
<?php
	if (!isset($_SESSION)) {
		session_start();
	}
	if(isset($_SESSION['id'])) {

		$id = $_SESSION['id'];

		include('connect_db.php');

		$result = mysql_query("SELECT Descricao, PrecoSemana, PrecoFimSemana FROM REFEICAO WHERE ID_Restaurante = $id");
		dbConnectionError($result, $connection);

		if(mysql_num_rows($result) > 0) {

			$data = mysql_fetch_array($result);

			if($data['Descricao'] == 'NULL')
				$description = "";
			else
				$description = $data['Descricao'];

			if($data['PrecoSemana'] == 'NULL')
				$priceWeek = "";
			else
				$priceWeek = $data['PrecoSemana'];
			
			// $priceWeek = $data['PrecoSemana'];
			if($data['PrecoFimSemana'] == 'NULL')
				$priceWeekend = "";
			else
				$priceWeekend = $data['PrecoFimSemana'];

		} else {

			$description = "";
			$priceWeek = "";
			$priceWeekend = "";
		}
	} else {
		header('Location: index.php');
		die();
	}
?>
<div class="w3-row divMain">
	<header class="w3-container w3-blue-grey">
		<h3>Prato do Dia</h3>
	</header>
	<div class="w3-container divEmail">
		<a style="text-decoration: none;" href="mailto:geral@pratododia.org">geral@pratododia.org</a>
	</div>
	<div class="w3-container divCenter">
		<!-- <form name="formlogin" id="formlogin" action="login.php" onsubmit="return loginValidation()" method="post"> -->
		<form class="w3-form" action="insert_description.php" method="post">
			<div class="w3-group">      
				<input class="w3-input" type="text" name="inputDescription" value="<?php echo $description;?>" required>
				<label class="w3-label">Descri&ccedil;&atilde;o da Refei&ccedil;&atilde;o</label>
				[Ex.: Sopa + Prato + Caf&eacute;]
			</div>
			<!-- <br> -->
			<div class="w3-group" style="margin-top: 4em;">
				<input class="w3-input" type="number" step="any" min="0" name="inputPriceWeek" value="<?php echo $priceWeek;?>" required>
				<label class="w3-label">Pre&ccedil;o (&euro;)</label>
				[Semana]
			</div>
			<div class="w3-group" style="margin-top: 4em;">
				<input class="w3-input" type="number" step="any" min="0" name="inputPriceWeekend" value="<?php echo $priceWeekend;?>">
				<label class="w3-label">Pre&ccedil;o (&euro;)</label>
				[Fim-de-semana]
			</div>
			<div style="text-align: right;">
				<button class="w3-btn w3-blue-grey" type="submit">Guardar</button>
			</div>
		</form>
	</div>
	<div class="w3-container divBottom">
		<div class="w3-border">
			<a class="w3-btn w3-blue-grey buttonBottom" href="menu.php">Voltar</a>
		</div>
	</div>
</div>
</body>
</html>