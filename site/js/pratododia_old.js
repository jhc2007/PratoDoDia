function getCities(district) {

	document.getElementById("selectCity").innerHTML = "";
	var xmlhttp;
	
	if (window.XMLHttpRequest) { // Mozilla, Safari, ...  
		xmlhttp = new XMLHttpRequest();  
	} else { // IE  
		try {  
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");  
		}   
		catch (e) {  
			try {  
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");  
			}   
			catch (e) {}  
		}  
	} 

	// var xmlhttp = new XMLHttpRequest();
	
	// xmlhttp.overrideMimeType('text/xml'); 
	xmlhttp.onreadystatechange = function() {
	  if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    document.getElementById("selectCity").innerHTML = 
	    xmlhttp.responseText;
	  }
	};
	var url = "http://pratododia.org/getcities.php?district=" + district;
	// alert(district);
	// xmlhttp.open("GET", url, true); //ASYNC
	xmlhttp.open("GET", url, false); //SYNC
	//xmlhttp.open('GET', 'getcities.php?district=' + district, true);
	xmlhttp.send(null);

}

function clearErrorMessage() {
	document.getElementById("pErrorMessage").innerHTML = "";
}

function emailValidation() {

	clearErrorMessage();

	var email = document.getElementById("inputEmail").value;
	// var password = document.getElementById("inputPassword").value;
	// alert("Email: " + email + "\n");
	// if(email == "" || password == "") {
	// 	document.getElementById("pLoginErrorMessage").innerHTML = "Inserir Email / Password";
	// 	return false;
	// }

	var emailPattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    if(!emailPattern.test(email)) {         
		document.getElementById("pErrorMessage").innerHTML = "Email inv&aacute;lido";
		return false;
    } 
	
	return true;
}

function wordValidation() {

	clearErrorMessage();

	var number = document.getElementById("inputNumberOfLetters").value;
	var word = document.getElementById("word").innerHTML;

	if(number != word.length) {
		document.getElementById("pErrorMessage").innerHTML = "N&uacute;mero de letras incorrecto";
		return false;
	}
	
	return true;
}

function newPasswordValidation() {

	clearErrorMessage();

	var actualPass = document.getElementById("inputActualPassword").value;
	var newPass = document.getElementById("inputNewPassword").value;

	if(actualPass == newPass) {
		document.getElementById("pErrorMessage").innerHTML = "Passwords actual e nova iguais";
		return false;
	}

	return true;
}

function passwordRepetitionValidation() {

	clearErrorMessage();

	var newPass = document.getElementById("inputNewPassword").value;
	var newPassRep = document.getElementById("inputNewPasswordRepetition").value;

    if(newPass != newPassRep) {         
		document.getElementById("pErrorMessage").innerHTML = "Repeti&ccedil;&atilde;o de password incorrecta";
		return false;
    } 
	
	return true;
}
