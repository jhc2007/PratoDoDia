package org.pratododia.pratododia;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class DateAndDay {

	private String date;
	private String day;
	
	public DateAndDay() {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH", java.util.Locale.getDefault());
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Lisbon"));
		Calendar calendar = Calendar.getInstance();

		if(Integer.valueOf(simpleDateFormat.format(calendar.getTime())) > 14)
			calendar.add(Calendar.DATE, 1);

		simpleDateFormat.applyPattern("yyyy-MM-dd");
		date = simpleDateFormat.format(calendar.getTime());

		day = String.valueOf(calendar.get(Calendar.DAY_OF_WEEK) - 1);
	}
	
	public String getDate() {
		return date;
	}
	
	public String getDay() {
		return day;
	}
}
