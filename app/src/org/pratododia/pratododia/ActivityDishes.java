package org.pratododia.pratododia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityDishes extends Activity {
	
	private TextView textViewDishesHeader;
	private TextView textViewDishesError;
	private ListView listViewDishes;
	private JSONArray jArray = null;
	private String city;
	private String tel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dishes);
		StrictMode.enableDefaults();
		
		textViewDishesHeader = (TextView)findViewById(R.id.textViewDishesHeader);
		textViewDishesError = (TextView)findViewById(R.id.textViewDishesError);
		listViewDishes = (ListView) findViewById(R.id.listViewDishes);
		
		String[] days = {"Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S�b" };
		String[] months = { "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez" };
		int weekend;
			
		Intent intent = getIntent();
		city = intent.getStringExtra("city");
		String date = intent.getStringExtra("date");
		String day = intent.getStringExtra("day");
		
		if(day.equals("0") || day.equals("6"))
			weekend = 1;
		else
			weekend = 0;
		
		if(getDishes(city, date, weekend)) {
			
		    String[] yearMonthDay = date.split("-");
			textViewDishesHeader.setText(city + " | " + days[Integer.valueOf(day)] + " " + yearMonthDay[2] + " " + months[Integer.valueOf(yearMonthDay[1]) - 1]);
			listViewDishes.setOnItemClickListener(new OnItemClickListener() {
		        @Override
		        public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 		        	
		        	if(position % 2 == 0) {
		        		try {
							JSONObject json = jArray.getJSONObject(position / 2);
							String message = "<u>" + (String)json.getString("Nome") + "</u>";
							int index = json.getString("Nome").length() + 1;
							if(json.has("Morada")) {
								message += "<br>" + (String)json.getString("Morada");
								index += json.getString("Morada").length() + 1;
							}
							message += "<br>" + city;
							index += city.length() + 6;
							if(json.has("Telefone"))
								message += "<br>Tel: " + (String)json.getString("Telefone");
							if(json.has("Descricao") || json.has("Preco")) {
								message += "<br><br><u>Prato do Dia</u>";
								if(json.has("Descricao"))
									message += "<br>" + (String)json.getString("Descricao");
								if(json.has("Preco"))
									message += "<br>" + (String)json.getString("Preco") + " �";
							}
							if(json.has("Telefone")) {
								tel = (String)json.getString("Telefone");
							    SpannableString messageSpannable = new SpannableString(Html.fromHtml(message));
							    ClickableSpan clickableSpan = new ClickableSpan() {
							        @Override
							        public void onClick(View widget) {							            
							        	Intent intentDial = new Intent(Intent.ACTION_DIAL);
							            intentDial.setData(Uri.parse("tel:" + tel));
							            startActivity(intentDial);
							            
							        }
							    };
							    messageSpannable.setSpan(clickableSpan, index, index + 9, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
							    descriptionAlertDialog(messageSpannable);
							} else {
								descriptionAlertDialog(Html.fromHtml(message));
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		        	}
		        }
	        });
		}
	}
	
	public void descriptionAlertDialog(Spanned message) {

		AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_DeviceDefault_Dialog));
		builder.setMessage(message)
		.setCancelable(false)
		.setNegativeButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();				
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
		TextView textViewMessage = (TextView) alert.findViewById(android.R.id.message);
		textViewMessage.setMovementMethod(LinkMovementMethod.getInstance());
		textViewMessage.setGravity(Gravity.CENTER);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_dishes, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_dishes_refresh) {
			
			finish();
			DateAndDay dateAndDay = new DateAndDay();
			Intent intent = new Intent(this, ActivityDishes.class);
            intent.putExtra("city", city);
            intent.putExtra("date", dateAndDay.getDate());
            intent.putExtra("day", dateAndDay.getDay());
            startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean getDishes(String city, String date, int weekend) {
		
		String response = "";
		HttpURLConnection connection = null;
		InputStream input = null;
		
		try {
			connection = (HttpURLConnection) new URL("http://pratododia.org/app2getdata.php?city=" + URLEncoder.encode(city, "UTF-8") + "&date=" + date + "&weekend=" + weekend).openConnection();
			input = connection.getInputStream();
		} catch (IOException e) {
			return setTextViewDishesErrorVisible("connection");
		}

		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(input, "iso-8859-1"), 8);
		    StringBuilder sb = new StringBuilder();
		    String line = null;
		    try {
				while((line = reader.readLine()) != null)
					sb.append(line + "\n");
				reader.close();
				input.close();
				response = sb.toString();
			} catch (IOException e) {
				return setTextViewDishesErrorVisible("connection");
			}
		    connection.disconnect();
		} catch (UnsupportedEncodingException e) {
			return setTextViewDishesErrorVisible("connection");
		}
		try {
			jArray = new JSONArray(response);

			String[] dishes = new String[jArray.length() * 2];
									
			for(int i=0; i<jArray.length(); i++) {
				JSONObject json = jArray.getJSONObject(i);
				dishes[i * 2] = (String)json.getString("Nome");
				dishes[i * 2 + 1] = (String)json.getString("Prato");
			}
			ColorArrayAdapter adapter = new ColorArrayAdapter(this, android.R.layout.simple_list_item_1, dishes); 
			listViewDishes.setAdapter(adapter);
			return true;
		} catch (JSONException e) {
			return setTextViewDishesErrorVisible("empty");
		}	
	}
	
	public boolean setTextViewDishesErrorVisible(String error) {
		
		textViewDishesError.setVisibility(View.VISIBLE);
		if(error.equals("connection"))
			textViewDishesError.setText(getResources().getString(R.string.textViewDBConnectionError));
		else
			textViewDishesError.setText(getResources().getString(R.string.textViewDBEmptyError));
		return false;
	}
}
