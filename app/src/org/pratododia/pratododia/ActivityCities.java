package org.pratododia.pratododia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ActivityCities extends Activity {

	private ListView listViewCities;
	private TextView textViewCitiesError; 
	private DateAndDay dateAndDay;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cities);
		StrictMode.enableDefaults();

		Intent intent = getIntent();
		String district = intent.getStringExtra("district");
		//String date = intent.getStringExtra("date");

		listViewCities = (ListView) findViewById(R.id.listViewCities);
		textViewCitiesError = (TextView) findViewById(R.id.textViewCitiesError);
			
		dateAndDay = new DateAndDay();
			
		if(getCities(district, dateAndDay.getDate())) {

			Toast.makeText(getApplicationContext(), "Selecione Concelho", Toast.LENGTH_SHORT).show();
			
			listViewCities.setOnItemClickListener(new OnItemClickListener() {
	            
		        @Override
		        public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 
		        	  // Item selecionado
		              String city = ((TextView) view).getText().toString();
		              //Log.d("DEBUG", city);
		              // Invocar nova atividade ap�s sele��o de item
		              Intent intent = new Intent(getApplicationContext(), ActivityDishes.class);
		              // Enviar dados para a nova atividade
		              intent.putExtra("city", city);
		              intent.putExtra("date", dateAndDay.getDate());
		              intent.putExtra("day", dateAndDay.getDay());
		              startActivity(intent);
		          }
	        });
		}
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_cities, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_cities_refresh) {
			
			//finish(); 
			//startActivity(getIntent());
			// or
			this.recreate();
			// or
			//this.onCreate(null);
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean getCities(String district, String date) {

		String response = "";
		HttpURLConnection connection = null;
		InputStream input = null;
		
		try {
			connection = (HttpURLConnection) new URL("http://pratododia.org/app2getcities.php?district=" + URLEncoder.encode(district, "UTF-8") + "&date=" + date).openConnection();
			input = connection.getInputStream();
		} catch (IOException e) {
			return setTextViewCitiesErrorVisible("connection");
		}

		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(input, "iso-8859-1"), 8);
		    StringBuilder sb = new StringBuilder();
		    String line = null;
		    try {
				while((line = reader.readLine()) != null)
					sb.append(line + "\n");
				reader.close();
				input.close();
				response = sb.toString();
			} catch (IOException e) {
				return setTextViewCitiesErrorVisible("connection");
			}
		    connection.disconnect();
		} catch (UnsupportedEncodingException e) {
			return setTextViewCitiesErrorVisible("connection");
		}
		
		try {
			JSONArray jArray = new JSONArray(response);

			String cities[] = new String[jArray.length()];
			for(int i=0; i<jArray.length(); i++) {
				JSONObject json = jArray.getJSONObject(i);
				cities[i] = json.getString("C");
			}
			listViewCities.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cities));
			//textViewCitiesError.setVisibility(View.GONE);
			return true;
		} catch (JSONException e) {
			return setTextViewCitiesErrorVisible("empty");
		}
	}
	
	public boolean setTextViewCitiesErrorVisible(String error) {
		textViewCitiesError.setVisibility(View.VISIBLE);
		if(error.equals("connection"))
			textViewCitiesError.setText(getResources().getString(R.string.textViewDBConnectionError));
		else
			textViewCitiesError.setText(getResources().getString(R.string.textViewDBEmptyError));
		return false;
	}
}
