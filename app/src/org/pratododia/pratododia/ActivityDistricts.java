package org.pratododia.pratododia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ActivityDistricts extends Activity {

	private ListView listViewDistricts;
	private TextView textViewDistrictsError; 
	private DateAndDay dateAndDay;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_districts);
		StrictMode.enableDefaults();
		
		if(!isOnline()) {
		
			connectionAlertDialog();
		
		} else {

			listViewDistricts = (ListView) findViewById(R.id.listViewDistricts);
			textViewDistrictsError = (TextView) findViewById(R.id.textViewDistrictsError);
			
			dateAndDay = new DateAndDay();
			
			if(getDistricts(dateAndDay.getDate())) {
				
				Toast.makeText(getApplicationContext(), "Selecione Distrito", Toast.LENGTH_SHORT).show();

				listViewDistricts.setOnItemClickListener(new OnItemClickListener() {
		            
			        @Override
			        public void onItemClick(AdapterView<?> parent, View view, int position, long id) { 
			        	  // Item selecionado
			              String district = ((TextView) view).getText().toString();
			              //int y = Arrays.asList(districts).indexOf('e');
			              //Log.d("DEBUG", city);
			              // Invocar nova atividade ap�s sele��o de item
			              Intent intent = new Intent(getApplicationContext(), ActivityCities.class);
			              // Enviar dados para a nova atividade
			              intent.putExtra("district", district);
			              //intent.putExtra("date", dateAndDay.getDate());
			              //intent.putExtra("day", dateAndDay.getDay());
			              startActivity(intent);
			          }
		        });
			}
		}
	}
	
	public boolean isOnline() {
		
		ConnectivityManager connManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connManager.getActiveNetworkInfo();
		return (netInfo != null && netInfo.isConnected());
	}
	
	
	public void connectionAlertDialog() {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, android.R.style.Theme_DeviceDefault_Dialog));
		builder.setMessage("Ative a liga��o � Internet\n")
		.setTitle("Sem Liga��o")
		.setCancelable(false)
		.setNegativeButton("Fechar", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
			}
		})
		.setPositiveButton("Repetir", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				recreate();
			}
		});
		
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_districts, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_districts_refresh) {
			
			//finish(); 
			//startActivity(getIntent());
			// or
			this.recreate();
			// or
			//this.onCreate(null);
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean getDistricts(String date) {

		String response = "";
		HttpURLConnection connection = null;
		InputStream input = null;
		
		try {
			connection = (HttpURLConnection) new URL("http://pratododia.org/app2getdistricts.php?date=" + date).openConnection();
			input = connection.getInputStream();
		} catch (IOException e) {
			return setTextViewDistrictsErrorVisible("connection");
		}

		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(input, "iso-8859-1"), 8);
		    StringBuilder sb = new StringBuilder();
		    String line = null;
		    try {
				while((line = reader.readLine()) != null)
					sb.append(line + "\n");
				reader.close();
				input.close();
				response = sb.toString();
			} catch (IOException e) {
				return setTextViewDistrictsErrorVisible("connection");
			}
		    connection.disconnect();
		} catch (UnsupportedEncodingException e) {
			return setTextViewDistrictsErrorVisible("connection");
		}

		/*
		if(response.equals(""))
			return setTextViewDistrictsErrorVisible("empty");
		
		String districts[] = response.split(",");
		listViewDistricts.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, districts));
		return true;
		*/
		
		try {
			JSONArray jArray = new JSONArray(response);

			String districts[] = new String[jArray.length()];
			for(int i=0; i<jArray.length(); i++) {
				JSONObject json = jArray.getJSONObject(i);
				districts[i] = json.getString("D");
			}
			listViewDistricts.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, districts));
			//textViewCitiesError.setVisibility(View.GONE);
			return true;
		} catch (JSONException e) {
			return setTextViewDistrictsErrorVisible("empty");
		}
		

	}
	
	public boolean setTextViewDistrictsErrorVisible(String error) {
		textViewDistrictsError.setVisibility(View.VISIBLE);
		if(error.equals("connection"))
			textViewDistrictsError.setText(getResources().getString(R.string.textViewDBConnectionError));
		else
			textViewDistrictsError.setText(getResources().getString(R.string.textViewDBEmptyError));
		return false;
	}
}
