package org.pratododia.pratododia;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class ColorArrayAdapter extends ArrayAdapter<String>{
 
    public ColorArrayAdapter(Context context, int textViewResourceId, String[] items) {
        super(context, textViewResourceId, items);
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView)super.getView(position, convertView, parent);
        view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        if(position % 2 == 0) {
            view.setBackgroundColor(Color.parseColor("#607D8B"));
            //view.setTypeface(Typeface.DEFAULT_BOLD);
        } else {
        	view.setBackgroundColor(Color.parseColor("#E0E0E0"));
        }
        return view;
    }
 
}
